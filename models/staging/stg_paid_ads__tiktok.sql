{{ config(materialized='view') }}


select
    cast(ad_id as string) as ad_id,
    add_to_cart,
    cast(adgroup_id as string) as adset_id,
    cast(campaign_id as string) as campaign_id,
    channel,
    clicks,
    cast (null as int) as comments,
    cast (null as string) as creative_id,
    date,
    cast (null as int) as engagements,
    impressions,
    rt_installs + skan_app_install as installs,
    cast (null as int) as likes,
    cast (null as int) as link_clicks,
    cast (null as string) as placement_id,
    cast (null as int) as post_click_conversions,
    cast (null as int) as post_view_conversions,
    cast (null as int) as posts,
    purchase,
    registrations,
    cast (null as int) as revenue,
    cast (null as int) as shares,
    spend,
    conversions + skan_conversion as total_conversions,
    video_views
from {{ ref("src_ads_tiktok_ads_all_data") }}