{{ config(materialized='view') }}

select
    cast(ad_id as string) as ad_id,
    cast (null as int) as add_to_cart,
    cast(adset_id as string) as adset_id,
    cast(campaign_id as string) as campaign_id,
    channel,
    clicks,
    cast (null as int) as comments,
    cast (null as string) as creative_id,
    date,
    cast (null as int) as engagements,
    imps as impressions,
    cast (null as int) as installs,
    cast (null as int) as likes,
    cast (null as int) as link_clicks,
    cast (null as string) as placement_id,
    cast (null as int) as post_click_conversions,
    cast (null as int) as post_view_conversions,
    cast (null as int) as posts,
    cast (null as int) as purchase,
    cast (null as int) as registrations,
    revenue,
    cast (null as int) as shares,
    spend,
    conv as total_conversions,
    cast (null as int) as video_views
from {{ ref("src_ads_bing_all_data") }}