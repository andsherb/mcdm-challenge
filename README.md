# Marketing common data modelling challenge


Project Organization
------------

    ├── README.md
	├── analyses
	├── seeds
 	│   ├── src_promoted_tweets_twitter_all_data.csv
 	│   ├── src_ads_tiktok_ads_all_data.csv
 	│   ├── src_ads_creative_facebook_all_data.csv
 	│   ├── src_ads_bing_all_data.csv
 	│   └── mcdm_paid_ads_basic_performance_structure.csv
 	├── dbt_project.yml
 	├── macros
 	├── models
 	│   ├── staging
 	│   │   ├── stg_paid_ads__twitter.sql
 	│   │   ├── stg_paid_ads__tiktok.sql
 	│   │   ├── stg_paid_ads__facebook.sql
 	│   │   ├── stg_paid_ads__bing.sql
 	│   │   ├── stg_paid_ads__basic_performance.sql
 	│   │   └── schema.yml
 	├── snapshots
 	└── tests
    


--------

# Used tools

- dbt Cloud
- Google Big Query
- Google Looker Studio

# Report 

Dashboard is situated in Google Looker Studio with hints:

- *Cost per engage* is just a spended sum divided by sum of engagements
- *Conversion cost* is calculated by dividing sum of spended by total conversions count
- *Impressions by channel* is a sum of impressions for each channel
- *CPC* gets like sum of spended divided by clicks count

Report link: https://lookerstudio.google.com/reporting/d66cb576-8879-46e7-9c54-f86288d02fdb

# How to add a new source

1) Add a new model to models/staging contains SQL query filling columns which are not in ```mcdm_paid_ads_basic_performance_structure.csv```
with nulls and cast existing columns to right type

2) Change a ```stg_paid_ads__basic_performance.sql``` model adding a union with query selecting all data from the table created in the first point

3) Try dbt build in dbt cloud